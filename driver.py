import logging
import time

from config import Config
from core import Module


ONE_SPEEN = 3


class Driver(Module):
    param_c: str = '000'

    def __init__(
        self,
        config: Config
    ):
        super().__init__(config)
        self.param: str = 'driver'
        self.param_a = config.param_a
        self.working: bool = None

    def run(self):
        """Выполняем циклически с периодичностью ONE_SPEEN действие пока запущен цикл программы
        Метод выводит в лог каждые 3 секунды имя модуля и значения 3-х параметров: param_a, param_b, param_c
        """
        name = self.get_name()
        logging.info('Thread [%s] started', name)
        while self.working:
            logging.info(name)
            logging.info('\t%s', self.param_a)
            logging.info('\t%s', self.param_b)
            logging.info('\t%s', self.param_c)
            time.sleep(ONE_SPEEN)
