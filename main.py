import logging
import signal
import sys
from functools import partial
from time import sleep

from config import Config
from core import check_folder_logs, init_logger
from driver import Driver


ONE_ROUND = 1


def interrupt(signal_number, frame, driver):  # pylint: disable=unused-argument
    """Ловим сигнала прерывания – CTRL+c и завершаем все потоки и подпрограммы"""
    name = driver.get_name()
    driver.working = False
    logging.debug('Run False')
    logging.info('Thread [%s] stopped', name)
    logging.info('Program stopped')
    sys.exit()


def main():
    check_folder = check_folder_logs()
    if check_folder is True:
        init_logger()

    logging.info('Program started')
    driver = Driver(config=Config)
    driver.working = True
    logging.debug('Run True')
    driver.launch()

    signal.signal(signal.SIGINT, partial(interrupt, driver=driver))
    while True:
        sleep(ONE_ROUND)


if __name__ == '__main__':
    main()
