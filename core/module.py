from threading import Thread

from config import Config


class Module:
    def __init__(
        self,
        config: Config
    ):
        self.param: str = 'module'
        self.param_b = config.param_b

    def get_name(self) -> str:
        """Возвращаем строку вида {имя класса}-{id класса}"""
        return f'{self.__class__.__name__}-{id(self)}'

    def launch(self):
        """Создаем и запускаем поток с именем get_name, целевая функция (target) – run класса наследника"""
        t = Thread(target=self.run, name=self.get_name())
        t.start()
