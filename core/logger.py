import logging
import os
from datetime import datetime
from logging.handlers import RotatingFileHandler


def check_folder_logs() -> bool:
    """Проверяем, существует ли папка для хранения логов"""
    if os.path.exists('logs') is not True:
        os.mkdir("logs")
    return True


def init_logger():
    """Инициализуруем логгер для записи логов в файл и вывод в консоль"""
    log = logging.getLogger(__name__)
    now = datetime.now()
    date_time = now.strftime('%Y%m%d_%H%M%S')

    file_log = logging.FileHandler(f'logs/log_{date_time}.log')
    file_log.setLevel(logging.INFO)
    console_out = logging.StreamHandler()

    logging.basicConfig(
        handlers=(file_log, console_out),
        level=logging.DEBUG,
        format='[%(levelname)s:%(asctime)s:%(filename)s:%(lineno)s] %(message)s',
        datefmt='%Y%m%d_%H%M%S'
    )
    file_rotating = RotatingFileHandler(f'logs/log_{date_time}.log', maxBytes=4096)
    file_rotating.setLevel(logging.INFO)
    log.addHandler(file_rotating)
