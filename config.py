from dataclasses import dataclass


@dataclass(frozen=True)
class Config:
    log_folder: str = 'logs'
    log_level: str = 'debug'

    param_a: bool = True
    param_b = ['a', 'b', 'c']
